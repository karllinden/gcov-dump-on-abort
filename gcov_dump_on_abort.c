/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#define fail(...) \
    do { \
        fprintf(stderr, __FILE__ ": " __VA_ARGS__); \
        exit(51); \
    } while (0);

extern void __gcov_dump(void);

void abort(void) {
    __gcov_dump();

    void (*real_abort)(void) = dlsym(RTLD_NEXT, "abort");
    if (real_abort == NULL) {
        fail("Cannot load next 'abort' symbol: %s\n", dlerror());
    }

    real_abort();
    fail("Real abort() returned.\n");
}
